package xyz.nebus.android

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.MobileAds

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        // Remove splash screen theme
        setTheme(R.style.AppTheme)

        MobileAds.initialize(this, R.string.ad_app_id.toString())

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
