package xyz.nebus.android.adapters

import android.content.Context
import android.widget.ArrayAdapter
import kotlin.math.min

class LimitedArrayAdapter(
    context: Context,
    textViewResourceId: Int,
    objects: Array<String>,
    private val limit: Int): ArrayAdapter<String>(context, textViewResourceId, objects) {

    override fun getCount(): Int {
        return min(this.limit, super.getCount())
    }
}