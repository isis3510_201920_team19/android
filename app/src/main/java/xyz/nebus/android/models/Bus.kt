package xyz.nebus.android.models

import com.mapbox.mapboxsdk.geometry.LatLng

data class Bus(
    val arrivesIn: Int,
    val course: Int,
    val location: LatLng,
    val speed: Double
)