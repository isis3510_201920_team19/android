package xyz.nebus.android.screens.main

import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import android.widget.SearchView
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.ads.MobileAds
import xyz.nebus.android.R
import xyz.nebus.android.adapters.LimitedArrayAdapter
import xyz.nebus.android.views.MapboxView
import xyz.nebus.android.databinding.MainFragmentBinding
import xyz.nebus.android.services.database.NebusDatabase
import xyz.nebus.android.views.AdMobView

class MainFragment : Fragment() {

    private lateinit var routeListViewAdapter: ArrayAdapter<String>
    private lateinit var busArrivalsListViewAdapter: ArrayAdapter<String>

    private lateinit var viewModel: MainViewModel
    private lateinit var viewModelFactory: MainViewModelFactory

    private lateinit var binding: MainFragmentBinding

    private lateinit var mapboxView: MapboxView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.main_fragment,
            container,
            false
        )

        val application = requireNotNull(this.activity).application
        val database = NebusDatabase.getInstance(application).nebusDatabaseDao

        viewModelFactory = MainViewModelFactory(database)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        binding.mainViewModel = viewModel
        binding.lifecycleOwner = this

        setupRouteListView()
        setupSearchViewListeners()
        setupBusArrivalListView()
        setupBackPressHandler()
        mapboxView = MapboxView(
            this,
            viewModel,
            savedInstanceState,
            binding.mapboxMapView,
            getString(R.string.access_token)
        )

        viewModel.mapboxView = mapboxView
        viewModel.initialLoad()

        viewModel.adMobView = AdMobView(
            getString(R.string.ad_unit_id),
            MobileAds.getRewardedVideoAdInstance(context),
            viewModel
        )

        return binding.root
    }

    private fun setupRouteListView() {
        setupRouteListViewAdapter()
        setupRouteListViewListeners()
    }

    private fun setupRouteListViewAdapter() {
        routeListViewAdapter = LimitedArrayAdapter(context!!, android.R.layout.simple_list_item_1, viewModel.busRouteList.value!!, 4)
        binding.routeListView.adapter = routeListViewAdapter

        viewModel.busRouteList.observe(this, Observer { routeList ->
            routeListViewAdapter = LimitedArrayAdapter(context!!, android.R.layout.simple_list_item_1, routeList, 4)
            binding.routeListView.adapter = routeListViewAdapter
            routeListViewAdapter.notifyDataSetChanged()
        })
    }

    private fun setupRouteListViewListeners() {
        binding.routeListView.setOnItemClickListener { _, _, position, _ ->
            val selectedRoute = routeListViewAdapter.getItem(position)!!
            binding.routeSearchView.setQuery(selectedRoute, false)
            binding.routeSearchView.clearFocus()

            viewModel.onSelectRoute(selectedRoute)
        }
    }

    private fun setupSearchViewListeners() {
        binding.routeSearchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                viewModel.onFocusOnRouteSearchView()
            } else {
                viewModel.onLeaveFocusFromRouteSearchView()
            }
        }

        binding.routeSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (routeListViewAdapter.count != 0) {
                    val firstResult = routeListViewAdapter.getItem(0)
                    binding.routeSearchView.setQuery(firstResult, false)
                    viewModel.onSelectRoute(firstResult!!)
                }
                binding.routeSearchView.clearFocus()
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                routeListViewAdapter.filter.filter(newText)
                return false
            }
        })
    }

    private fun setupBusArrivalListView() {
        setupBusArrivalListViewAdapter()
        setupBusArrivalListViewListeners()
    }

    private fun setupBusArrivalListViewAdapter() {
        busArrivalsListViewAdapter = LimitedArrayAdapter(context!!, android.R.layout.simple_list_item_1, viewModel.busArrivalList.value!!, 4)
        binding.busArrivalListView.adapter = busArrivalsListViewAdapter

        viewModel.busArrivalList.observe(this, Observer { busArrivalList ->
            busArrivalsListViewAdapter = LimitedArrayAdapter(context!!, android.R.layout.simple_list_item_1, busArrivalList, 4)
            binding.busArrivalListView.adapter = busArrivalsListViewAdapter
            busArrivalsListViewAdapter.notifyDataSetChanged()
        })
    }

    private fun setupBusArrivalListViewListeners() {
        binding.busArrivalListView.setOnItemClickListener { _, _, _, id ->
            viewModel.onSelectBusArrival(id+1)
        }
    }

    private fun setupBackPressHandler() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            viewModel.backPressed()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        mapboxView.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onResume() {
        super.onResume()
        binding.constraintLayout.requestFocus()
        if (viewModel.hadConnectionError()) {
            viewModel.retryLoading()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mapboxView.onDestroyView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapboxView.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapboxView.onLowMemory()
    }
}
