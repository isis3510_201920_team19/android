package xyz.nebus.android.screens.main

import android.location.Location
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.mapbox.mapboxsdk.geometry.LatLng
import xyz.nebus.android.models.Bus
import xyz.nebus.android.services.BusRouteService
import xyz.nebus.android.services.BusStopService
import xyz.nebus.android.services.PointsService
import xyz.nebus.android.services.UserService
import xyz.nebus.android.services.database.NebusDatabaseDao
import xyz.nebus.android.services.database.tables.BusStop
import xyz.nebus.android.views.AdMobView
import xyz.nebus.android.views.MapboxView

class MainViewModel(database: NebusDatabaseDao) : ViewModel() {

    enum class State {
        INITIAL_LOAD,
        INITIAL_LOAD_ERROR,
        WAITING_FOR_USER_ACTION,
        SELECTING_BUS_ROUTE,
        LOADING_BUS_STOPS,
        LOADING_BUS_STOPS_ERROR,
        SELECTING_BUS_STOP,
        LOADING_BUS_ARRIVALS,
        LOADING_BUS_ARRIVALS_ERROR,
        SELECTING_BUS_ARRIVAL,
        USING_POINTS,
        NOT_ENOUGH_POINTS,
        LOADING_AD,
        LOADING_AD_ERROR,
        LOADING_BUS_ARRIVAL,
        LOADING_BUS_ARRIVAL_ERROR,
        SHOWING_BUS_ARRIVAL
    }

    private lateinit var currentState: State

    private val userService = UserService(database)
    private val pointsService = PointsService(database)
    private val busRouteService = BusRouteService(database)
    private val busStopService = BusStopService(database)

    private val _pointCount = MutableLiveData<Int>()
    val pointCount: LiveData<Int>
        get() = _pointCount

    private val _routeLoadingProgess = MutableLiveData<Int>()
    val routeLoadingProgress: LiveData<Int>
        get() = _routeLoadingProgess

    private val _showInitialLoadProgress = MutableLiveData<Boolean>()
    val showInitialLoadProgress: LiveData<Boolean>
        get() = _showInitialLoadProgress

    private val _showConnectionError = MutableLiveData<Boolean>()
    val showConnectionError: LiveData<Boolean>
        get() = _showConnectionError

    private val _showLoadingView = MutableLiveData<Boolean>()
    val showLoadingView: LiveData<Boolean>
        get() = _showLoadingView

    private val _showRouteSearchView = MutableLiveData<Boolean>()
    val showRouteSearchView: LiveData<Boolean>
        get() = _showRouteSearchView

    private val _showRouteList = MutableLiveData<Boolean>()
    val showRouteList: LiveData<Boolean>
        get() = _showRouteList

    private var selectedBusRouteId: Long? = null
    private var selectedBusRouteName: String? = null

    private val _busRouteList = MutableLiveData<Array<String>>()
    val busRouteList: LiveData<Array<String>>
        get() = _busRouteList

    var mapboxView: MapboxView? = null

    private val _busStopList = MutableLiveData<Array<BusStop>>()
    val busStopList: LiveData<Array<BusStop>>
        get() = _busStopList

    private var selectedBusStopName: String? = null

    private val _showBusArrivalList = MutableLiveData<Boolean>()
    val showBusArrivalList: LiveData<Boolean>
        get() = _showBusArrivalList

    private val _showNotEnoughPointsDialog = MutableLiveData<Boolean>()
    val showNotEnoughPointsDialog: LiveData<Boolean>
        get() = _showNotEnoughPointsDialog

    var adMobView: AdMobView? = null

    private var adFinishedLoading: Boolean = false
    private var adLoadedCorrectly: Boolean = false

    private var selectedBusArrivalId: String? = null

    private var busArrivalListFirebaseRegistration: ListenerRegistration? = null
    private var busArrivalFirebaseRegistration: ListenerRegistration? = null

    private val _busArrivalList = MutableLiveData<Array<String>>()
    val busArrivalList: LiveData<Array<String>>
        get() = _busArrivalList

    init {
        setState(State.INITIAL_LOAD)

        _pointCount.value = 0
        _busRouteList.value = emptyArray()
        _busStopList.value = emptyArray()
        _busArrivalList.value = arrayOf("(4:22)", "(4:32)", "(4:42)", "(4:52)", "(5:02)")
    }

    private fun setState(state: State) {
        if (state < State.SELECTING_BUS_ARRIVAL) {
            busArrivalListFirebaseRegistration?.remove()
            busArrivalFirebaseRegistration?.remove()
        }

        currentState = state

        _showInitialLoadProgress.value = false
        _showConnectionError.value = false
        _showLoadingView.value = false
        _showRouteSearchView.value = true
        _showRouteList.value = false
        _showBusArrivalList.value = false
        _showNotEnoughPointsDialog.value = false
        when (state) {
            State.INITIAL_LOAD -> {
                _routeLoadingProgess.value = 0
                _showInitialLoadProgress.value = true
                _showRouteSearchView.value = false
                loadPointCount()
                registerUser()
                loadBusRoutes()
            }
            State.INITIAL_LOAD_ERROR -> {
                _showRouteSearchView.value = false
                _showConnectionError.value = true
            }
            State.WAITING_FOR_USER_ACTION -> {

            }
            State.SELECTING_BUS_ROUTE -> {
                _showRouteList.value = true
            }
            State.LOADING_BUS_STOPS -> {
                _showLoadingView.value = true
                loadBusRouteBusStops(selectedBusRouteId)
            }
            State.LOADING_BUS_STOPS_ERROR -> {
                _showConnectionError.value = true
            }
            State.SELECTING_BUS_STOP -> {

            }
            State.LOADING_BUS_ARRIVALS -> {
                _showLoadingView.value = true
                loadBusArrivals(selectedBusRouteName, selectedBusStopName)
            }
            State.LOADING_BUS_ARRIVALS_ERROR -> {
                _showConnectionError.value = true
            }
            State.SELECTING_BUS_ARRIVAL -> {
                _showBusArrivalList.value = true
            }
            State.USING_POINTS -> {
                _showBusArrivalList.value = true
                usePoints()
            }
            State.NOT_ENOUGH_POINTS -> {
                _showNotEnoughPointsDialog.value = true
            }
            State.LOADING_AD -> {
                _showLoadingView.value = true
                adMobView?.loadAd()
            }
            State.LOADING_AD_ERROR -> {
                _showConnectionError.value = true
            }
            State.LOADING_BUS_ARRIVAL -> {
                _showLoadingView.value = true
                loadBusArrival(selectedBusRouteName, selectedBusStopName, selectedBusArrivalId)
            }
            State.LOADING_BUS_ARRIVAL_ERROR -> {
                _showConnectionError.value = true
            }
            State.SHOWING_BUS_ARRIVAL -> {

            }
        }
    }

    fun backPressed() {
        when (currentState) {
            State.LOADING_BUS_STOPS -> {
                setState(State.WAITING_FOR_USER_ACTION)
            }
            State.LOADING_BUS_STOPS_ERROR -> {
                setState(State.WAITING_FOR_USER_ACTION)
            }
            State.SELECTING_BUS_STOP -> {
                setState(State.WAITING_FOR_USER_ACTION)
            }
            State.LOADING_BUS_ARRIVALS -> {
                setState(State.LOADING_BUS_STOPS)
            }
            State.LOADING_BUS_ARRIVALS_ERROR -> {
                setState(State.LOADING_BUS_STOPS)
            }
            State.SELECTING_BUS_ARRIVAL -> {
                setState(State.LOADING_BUS_STOPS)
            }
            State.NOT_ENOUGH_POINTS -> {
                setState(State.LOADING_BUS_ARRIVALS)
            }
            State.LOADING_AD -> {
                setState(State.LOADING_BUS_ARRIVALS)
            }
            State.LOADING_AD_ERROR -> {
                setState(State.LOADING_BUS_ARRIVALS)
            }
            State.LOADING_BUS_ARRIVAL -> {
                setState(State.LOADING_BUS_ARRIVALS)
            }
            State.LOADING_BUS_ARRIVAL_ERROR -> {
                setState(State.LOADING_BUS_ARRIVALS)
            }
            State.SHOWING_BUS_ARRIVAL -> {
                setState(State.LOADING_BUS_ARRIVALS)
            }
        }
    }

    fun hadConnectionError(): Boolean {
        return currentState == State.INITIAL_LOAD_ERROR
                || currentState == State.LOADING_BUS_STOPS_ERROR
                || currentState == State.LOADING_BUS_ARRIVALS_ERROR
    }

    fun retryLoading() {
        when (currentState) {
            State.INITIAL_LOAD_ERROR -> setState(State.INITIAL_LOAD)
            State.LOADING_BUS_STOPS_ERROR -> setState(State.LOADING_BUS_STOPS)
            State.LOADING_BUS_ARRIVALS_ERROR -> setState(State.LOADING_BUS_ARRIVALS)
            State.LOADING_AD_ERROR -> setState(State.LOADING_AD)
            State.LOADING_BUS_ARRIVAL_ERROR -> setState(State.LOADING_BUS_ARRIVAL)
        }
    }

    fun initialLoad() {
        setState(State.INITIAL_LOAD)
    }

    private fun registerUser() {
        userService.registerUser { result ->
            if (result.isSuccessful) {
                _routeLoadingProgess.value = _routeLoadingProgess.value!! + 50
            } else {
                setState(State.INITIAL_LOAD_ERROR)
            }
            finishInitialLoad()
        }
    }

    private fun loadBusRoutes() {
        busRouteService.loadBusRoutes { result, busRoutes ->
            if (result.isSuccessful) {
                _routeLoadingProgess.value = _routeLoadingProgess.value!! + 50
                val routeNames = busRoutes.map { busRoute -> busRoute.name }
                _busRouteList.value = routeNames.toTypedArray()
            } else {
                setState(State.INITIAL_LOAD_ERROR)
            }
            finishInitialLoad()
        }
    }

    private fun loadPointCount() {
        pointsService.getPoints { result, pointCount ->
            if (result.isSuccessful) {
                _pointCount.value = pointCount
            }
        }
    }

    private fun finishInitialLoad() {
        if (_routeLoadingProgess.value == 100) {
            setState(State.WAITING_FOR_USER_ACTION)
        }
    }

    fun onFocusOnRouteSearchView() {
        setState(State.SELECTING_BUS_ROUTE)
    }

    fun onLeaveFocusFromRouteSearchView() {
        setState(State.WAITING_FOR_USER_ACTION)
    }

    fun onSelectRoute(busRouteName: String) {
        selectedBusRouteId = _busRouteList.value!!.indexOf(busRouteName).toLong()
        selectedBusRouteName = busRouteName
        setState(State.LOADING_BUS_STOPS)
    }

    private fun loadBusRouteBusStops(busRouteId: Long?) {
        if (busRouteId != null) {
            busStopService.loadBusRouteBusStops(busRouteId + 1) { result, busStops ->
                if (result.isSuccessful) {
                    _busStopList.value = busStops.toTypedArray()
                    mapboxView?.setBusStops(_busStopList.value!!)
                    if (mapboxView?.originLocation != null) {
                        val closestBusStopName = getClosestBusStop(mapboxView?.originLocation)
                        if (closestBusStopName != null) {
                            mapboxView?.selectBusStop(closestBusStopName)
                            onSelectBusStop(closestBusStopName)
                            setState(State.LOADING_BUS_ARRIVALS)
                        } else {
                            setState(State.LOADING_BUS_STOPS_ERROR)
                        }
                    } else {
                        setState(State.SELECTING_BUS_STOP)
                    }
                } else {
                    setState(State.LOADING_BUS_STOPS_ERROR)
                }
            }
        }
    }

    private fun getClosestBusStop(location: Location?): String? {
        val busStops = _busStopList.value!!
        if (!busStops.isEmpty() && location != null) {
            var closestBusStop = busStops[0]
            var smallestDistance = Double.MAX_VALUE
            var distance = 0.0

            for (busStop in busStops) {
                distance = distanceBetween(location.latitude, location.longitude, busStop.latitude, busStop.longitude)
                if (distance < smallestDistance) {
                    smallestDistance = distance
                    closestBusStop = busStop
                }
            }

            return closestBusStop.name
        }
        return null
    }

    private fun distanceBetween(startLatitude: Double, startLongitude: Double, endLatitude: Double, endLongitude: Double): Double {
        return (endLatitude - startLatitude)*(endLatitude - startLatitude) + (endLongitude - startLongitude)*(endLongitude - startLongitude)
    }

    fun onSelectBusStop(busStopName: String) {
        selectedBusStopName = busStopName
        setState(State.LOADING_BUS_ARRIVALS)
    }

    private fun loadBusArrivals(busRouteName: String?, busStopName: String?) {
        busArrivalListFirebaseRegistration?.remove()
        busArrivalFirebaseRegistration?.remove()

        if (busRouteName != null && busStopName != null) {
            val db = FirebaseFirestore.getInstance()
            busArrivalListFirebaseRegistration = db.collection("bus_stops")
                .document(busStopName)
                .collection(busRouteName)
                .addSnapshotListener { querySnapshot, e ->
                    if (e != null) {
                        setState(State.LOADING_BUS_ARRIVALS_ERROR)
                        return@addSnapshotListener
                    }

                    if (querySnapshot != null && !querySnapshot.isEmpty) {
                        var arrivalList = ArrayList<String>()

                        for (document in querySnapshot.documents) {
                            arrivalList.add(document.get("arrivesIn").toString() + " min")
                        }

                        _busArrivalList.value = arrivalList.toTypedArray()
                        if (currentState == State.LOADING_BUS_ARRIVALS) {
                            setState(State.SELECTING_BUS_ARRIVAL)
                        }
                    } else {
                        setState(State.LOADING_BUS_ARRIVALS_ERROR)
                    }
                }
        }
    }

    fun onSelectBusArrival(id: Long) {
        selectedBusArrivalId = id.toString()
        setState(State.USING_POINTS)
    }

    private fun usePoints() {
        pointsService.usePoints(10) { result, pointCount ->
            if (result.isSuccessful) {
                _pointCount.value = pointCount
                setState(State.LOADING_BUS_ARRIVAL)
            } else {
                adFinishedLoading = false
                adLoadedCorrectly = false
                setState(State.NOT_ENOUGH_POINTS)
            }
        }
    }

    fun showAd() {
        setState(State.LOADING_AD)
    }

    fun adLoadedCorrectly() {
        adMobView?.showAd()
    }

    fun adFailedToLoad() {
        setState(State.LOADING_AD_ERROR)
    }

    fun adViewed() {
        pointsService.addPoints(10) { result, pointCount ->
            if (result.isSuccessful) {
                _pointCount.value = pointCount
                setState(State.USING_POINTS)
            }
        }
    }

    fun loadBusArrival(busRouteName: String?, busStopName: String?, busArrivalId: String?) {
        busArrivalFirebaseRegistration?.remove()

        if (busStopName != null && busRouteName != null && busArrivalId != null) {
            val db = FirebaseFirestore.getInstance()
            busArrivalFirebaseRegistration = db.collection("bus_stops")
                .document(busStopName)
                .collection(busRouteName)
                .document(busArrivalId)
                .addSnapshotListener { documentSnapshot, e ->
                    if (e != null) {
                        setState(State.LOADING_BUS_ARRIVAL_ERROR)
                        return@addSnapshotListener
                    }

                    if (documentSnapshot != null) {
                        val location = documentSnapshot.getGeoPoint("location")
                        val bus = Bus(
                            documentSnapshot["arrivesIn"].toString().toInt(),
                            documentSnapshot["course"].toString().toInt(),
                            LatLng(location!!.latitude, location!!.longitude),
                            documentSnapshot["speed"].toString().toDouble()
                        )
                        mapboxView?.clearNonSelectedBusStopMarkers()
                        mapboxView?.addBusMarker(bus)
                        if (currentState == State.LOADING_BUS_ARRIVAL) {
                            setState(State.SHOWING_BUS_ARRIVAL)
                        }
                    } else {
                        setState(State.LOADING_BUS_ARRIVAL_ERROR)
                    }
                }
        }
    }
}
