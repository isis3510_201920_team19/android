package xyz.nebus.android.screens.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import xyz.nebus.android.services.database.NebusDatabaseDao
import java.lang.IllegalArgumentException

class MainViewModelFactory(
    private val databaseDao: NebusDatabaseDao
) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(databaseDao) as T
        }
        throw IllegalArgumentException("Unkown ViewModel class")
    }
}