package xyz.nebus.android.services

import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.nebus.android.services.api.RetrofitClient
import xyz.nebus.android.services.api.models.GetBusRoutesResponse
import xyz.nebus.android.services.database.NebusDatabaseDao
import xyz.nebus.android.services.database.tables.BusRoute

class BusRouteService(private val database: NebusDatabaseDao) {

    private var busRouteServiceJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + busRouteServiceJob)

    data class BusRouteServiceResult(val isSuccessful: Boolean)

    fun loadBusRoutes(callback: (result: BusRouteServiceResult, busRoutes: ArrayList<BusRoute>) -> Unit) {
        uiScope.launch {
            val databaseBusRoutes = getBusRoutesFromDatabase()
            var alreadyReturnedBusRoutes = false

            if (databaseBusRoutes.isNotEmpty()) {
                var busRoutes = ArrayList<BusRoute>()
                var busRoute: BusRoute

                for (databaseBusRoute in databaseBusRoutes) {
                    busRoute = BusRoute(id = databaseBusRoute.id, name = databaseBusRoute.name)
                    busRoutes.add(busRoute)
                }
                callback(BusRouteServiceResult(true), busRoutes)
                alreadyReturnedBusRoutes = true
            }

            RetrofitClient.instance.getBusRoutes().enqueue(
                object: Callback<GetBusRoutesResponse> {

                    override fun onResponse(
                        call: Call<GetBusRoutesResponse>,
                        response: Response<GetBusRoutesResponse>
                    ) {
                        val responseBody = response.body()

                        if (responseBody != null) {
                            var busRoutes = ArrayList<BusRoute>()
                            var busRoute: BusRoute

                            for (getBusRoute in responseBody.busRoutes) {
                                busRoute = BusRoute(id = getBusRoute.id, name = getBusRoute.name)
                                busRoutes.add(busRoute)
                            }

                            uiScope.launch {
                                updateBusRoutesInDatabase(busRoutes)
                                if (!alreadyReturnedBusRoutes) {
                                    callback(BusRouteServiceResult(true), busRoutes)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<GetBusRoutesResponse>, t: Throwable) {
                        if (!alreadyReturnedBusRoutes) {
                            callback(BusRouteServiceResult(false), ArrayList())
                        }
                    }
                }
            )
        }
    }

    private suspend fun getBusRoutesFromDatabase(): List<BusRoute> {
        return withContext(Dispatchers.IO) {
            val busRoutes = database.getBusRoutes()
            busRoutes
        }
    }

    private suspend fun updateBusRoutesInDatabase(busRoutes: ArrayList<BusRoute>) {
        withContext(Dispatchers.IO) {
            database.insertBusRoutes(busRoutes)
        }
    }
}