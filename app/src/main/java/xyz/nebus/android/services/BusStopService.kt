package xyz.nebus.android.services

import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.nebus.android.services.api.RetrofitClient
import xyz.nebus.android.services.api.models.GetBusStopsResponse
import xyz.nebus.android.services.database.NebusDatabaseDao
import xyz.nebus.android.services.database.tables.BusRouteBusStop
import xyz.nebus.android.services.database.tables.BusStop

class BusStopService(private val database: NebusDatabaseDao) {

    private var busStopServiceJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + busStopServiceJob)

    data class BusStopServiceResult(val isSuccessful: Boolean)

    fun loadBusRouteBusStops(busRouteId: Long, callback: (result: BusStopServiceResult, busStops: ArrayList<BusStop>) -> Unit) {
        uiScope.launch {
            val databaseBusStops = getBusRouteBusStopsFromDatabase(busRouteId)
            var alreadyReturnedBusRouteBusStops = false

            if (databaseBusStops.isNotEmpty()) {
                var busRouteBusStops = ArrayList<BusStop>()
                var busStop: BusStop

                for (databaseBusStop in databaseBusStops) {
                    busStop = BusStop(
                        id = databaseBusStop.id,
                        name = databaseBusStop.name,
                        latitude = databaseBusStop.latitude,
                        longitude = databaseBusStop.longitude
                    )
                    busRouteBusStops.add(busStop)
                }
                callback(BusStopServiceResult(true), busRouteBusStops)
                alreadyReturnedBusRouteBusStops = true
            }

            RetrofitClient.instance.getBusStops(busRouteId).enqueue(
                object: Callback<GetBusStopsResponse> {

                    override fun onResponse(
                        call: Call<GetBusStopsResponse>,
                        response: Response<GetBusStopsResponse>
                    ) {
                        val responseBody = response.body()

                        if (responseBody != null) {
                            var busStops = ArrayList<BusStop>()
                            var busStop: BusStop

                            for (getBusStop in responseBody.busStops) {
                                busStop = BusStop(
                                    getBusStop.id,
                                    getBusStop.name,
                                    getBusStop.latitude,
                                    getBusStop.longitude
                                )
                                busStops.add(busStop)
                            }

                            uiScope.launch {
                                updateBusRouteBusStopsInDatabase(busRouteId, busStops)
                                if (!alreadyReturnedBusRouteBusStops) {
                                    callback(BusStopServiceResult(true), busStops)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<GetBusStopsResponse>, t: Throwable) {
                        if (!alreadyReturnedBusRouteBusStops) {
                            callback(BusStopServiceResult(false), ArrayList())
                        }
                    }
                }
            )
        }
    }

    private suspend fun getBusRouteBusStopsFromDatabase(busRouteId: Long): List<BusStop> {
        return withContext(Dispatchers.IO) {
            val busStops = database.getBusRouteBusStops(busRouteId)
            busStops
        }
    }

    private suspend fun updateBusRouteBusStopsInDatabase(busRouteId: Long, busStops: ArrayList<BusStop>) {
        var busRouteBusStops = ArrayList<BusRouteBusStop>()
        var busRouteBusStop: BusRouteBusStop

        for (busStop in busStops) {
            busRouteBusStop = BusRouteBusStop(busRouteId, busStop.id)
            busRouteBusStops.add(busRouteBusStop)
        }

        withContext(Dispatchers.IO) {
            database.insertBusStops(busStops)
            database.insertBusRouteBusStops(busRouteBusStops)
        }
    }
}