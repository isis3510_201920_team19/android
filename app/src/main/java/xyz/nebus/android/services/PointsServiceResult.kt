package xyz.nebus.android.services

import kotlinx.coroutines.*
import xyz.nebus.android.services.database.NebusDatabaseDao
import xyz.nebus.android.services.database.tables.Points

class PointsService(private val database: NebusDatabaseDao) {

    private var pointsServiceJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + pointsServiceJob)

    data class PointsServiceResult(val isSuccessful: Boolean)

    fun getPoints(callback: (result: PointsServiceResult, pointCount: Int) -> Unit) {
        uiScope.launch {
            val points = getPointCountFromDatabase()
            callback(PointsServiceResult(true), points)
        }
    }

    fun addPoints(addedPoints: Int, callback: (result: PointsServiceResult, pointCount: Int) -> Unit) {
        uiScope.launch {
            var points = getPointCountFromDatabase()
            points += addedPoints
            setPointCountInDatabase(points)
            callback(PointsServiceResult(true), points)
        }
    }

    fun usePoints(usedPoints: Int, callback: (result: PointsServiceResult, pointCount: Int) -> Unit) {
        uiScope.launch {
            var points = getPointCountFromDatabase()
            if (points < usedPoints) {
                callback(PointsServiceResult(false), points)
            } else {
                points -= usedPoints
                setPointCountInDatabase(points)
                callback(PointsServiceResult(true), points)
            }
        }
    }

    private suspend fun getPointCountFromDatabase(): Int {
        return withContext(Dispatchers.IO) {
            val points = database.getPointCount()
            if (points != null) {
                points.pointCount
            } else {
                val newPoints = Points()
                database.insertPoints(newPoints)
                newPoints.pointCount
            }
        }
    }

    private suspend fun setPointCountInDatabase(pointCount: Int) {
        withContext(Dispatchers.IO) {
            val points = database.getPointCount()
            points.pointCount = pointCount
            database.updatePointCount(points)
        }
    }
}