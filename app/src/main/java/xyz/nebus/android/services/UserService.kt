package xyz.nebus.android.services

import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.nebus.android.services.api.RetrofitClient
import xyz.nebus.android.services.api.models.CreateUserRequest
import xyz.nebus.android.services.api.models.CreateUserResponse
import xyz.nebus.android.services.database.NebusDatabaseDao
import xyz.nebus.android.services.database.tables.User

class UserService(private val database: NebusDatabaseDao) {

    private var auth = FirebaseAuth.getInstance()

    private var userServiceJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + userServiceJob)

    data class UserServiceResult(val isSuccessful: Boolean)

    fun registerUser(callback: (result: UserServiceResult) -> Unit) {
        uiScope.launch {
            val databaseUser = getUserFromDatabase()

            if (databaseUser == null) {
                var currentUser = auth.currentUser

                if (currentUser == null) {
                    auth.signInAnonymously().addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            currentUser = auth.currentUser
                            registerBackendUser(auth.currentUser!!.uid, callback)
                        } else {
                            callback(UserServiceResult(false))
                        }
                    }
                } else {
                    registerBackendUser(currentUser!!.uid, callback)
                }
            } else {
                callback(UserServiceResult(true))
            }
        }
    }

    private fun registerBackendUser(uid: String, callback: (result: UserServiceResult) -> Unit) {
        val createUserRequest = CreateUserRequest(uid)
        RetrofitClient.instance.createUser(createUserRequest).enqueue(
            object: Callback<CreateUserResponse> {

                override fun onResponse(
                    call: Call<CreateUserResponse>,
                    response: Response<CreateUserResponse>
                ) {
                    val responseBody = response.body()
                    if (responseBody != null) {
                        val user = User(
                            firebaseId = uid,
                            backendId = responseBody.id
                        )
                        uiScope.launch {
                            insertUserToDatabase(user)
                            callback(UserServiceResult(true))
                        }
                    }
                }

                override fun onFailure(call: Call<CreateUserResponse>, t: Throwable) {
                    callback(UserServiceResult(false))
                }
            }
        )
    }

    private suspend fun getUserFromDatabase(): User? {
        return withContext(Dispatchers.IO) {
            val user = database.getUser()
            user
        }
    }

    private suspend fun insertUserToDatabase(user: User) {
        withContext(Dispatchers.IO) {
            database.insertUser(user)
        }
    }
}
