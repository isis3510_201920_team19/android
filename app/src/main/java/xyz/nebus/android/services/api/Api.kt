package xyz.nebus.android.services.api

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import xyz.nebus.android.services.api.models.CreateUserRequest
import xyz.nebus.android.services.api.models.CreateUserResponse
import xyz.nebus.android.services.api.models.GetBusRoutesResponse
import xyz.nebus.android.services.api.models.GetBusStopsResponse

interface Api {

    // User
    @POST("user")
    fun createUser(
        @Body createUserRequest: CreateUserRequest
    ): Call<CreateUserResponse>

    // Bus Routes
    @GET("bus-routes")
    fun getBusRoutes(): Call<GetBusRoutesResponse>

    // Bus Stops
    @GET("bus-routes/{busRouteId}/bus-stops")
    fun getBusStops(@Path("busRouteId") busRouteId: Long): Call<GetBusStopsResponse>
}