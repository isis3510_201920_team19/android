package xyz.nebus.android.services.api.models

data class CreateUserRequest(val firebaseId: String)