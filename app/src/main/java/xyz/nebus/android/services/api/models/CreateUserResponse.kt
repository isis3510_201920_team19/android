package xyz.nebus.android.services.api.models

data class CreateUserResponse(
    val id: Long,
    val firebaseId: String
)