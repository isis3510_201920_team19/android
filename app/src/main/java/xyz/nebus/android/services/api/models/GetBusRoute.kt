package xyz.nebus.android.services.api.models

data class GetBusRoute(
    val id: Long,
    val name: String
)