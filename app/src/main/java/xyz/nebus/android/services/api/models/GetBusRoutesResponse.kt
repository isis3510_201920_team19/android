package xyz.nebus.android.services.api.models

data class GetBusRoutesResponse(
    val busRoutes: List<GetBusRoute>
)