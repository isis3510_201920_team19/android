package xyz.nebus.android.services.api.models

data class GetBusStop (
    val id: Long,
    val name: String,
    val latitude: Double,
    val longitude: Double
)