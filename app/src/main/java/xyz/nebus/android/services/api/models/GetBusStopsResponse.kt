package xyz.nebus.android.services.api.models

data class GetBusStopsResponse (
    val busStops: List<GetBusStop>
)