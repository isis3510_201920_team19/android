package xyz.nebus.android.services.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import xyz.nebus.android.services.database.tables.*

@Database(entities = [User::class, Points::class, BusRoute::class, BusStop::class, BusRouteBusStop::class], version = 1, exportSchema = false)
abstract class NebusDatabase: RoomDatabase() {

    abstract val nebusDatabaseDao: NebusDatabaseDao

    companion object {

        @Volatile
        private var INSTANCE: NebusDatabase? = null

        fun getInstance(context: Context) : NebusDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        NebusDatabase::class.java,
                        "nebus_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }

                return instance
            }
        }
    }
}