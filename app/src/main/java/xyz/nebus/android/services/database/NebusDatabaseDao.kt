package xyz.nebus.android.services.database

import androidx.room.*
import xyz.nebus.android.services.database.tables.*

@Dao
interface NebusDatabaseDao {

    // User
    @Insert
    fun insertUser(user: User): Long

    @Update
    fun updateUser(user: User)

    @Query("SELECT * FROM users LIMIT 1")
    fun getUser(): User?

    // Points
    @Insert
    fun insertPoints(points: Points): Long

    @Update
    fun updatePointCount(points: Points)

    @Query("SELECT * FROM points")
    fun getPointCount(): Points

    // Bus Routes
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBusRoutes(busRoute: List<BusRoute>)

    @Query("SELECT * FROM bus_routes")
    fun getBusRoutes(): List<BusRoute>

    // Bus Stops
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBusStops(busRoute: List<BusStop>)

    @Query("SELECT id, name, latitude, longitude FROM bus_stops AS a INNER JOIN (SELECT * FROM bus_routes_bus_stops WHERE busRouteId = :busRouteId) AS b ON a.id = b.busStopId")
    fun getBusRouteBusStops(busRouteId: Long): List<BusStop>

    // Bus Route Bus Routes
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBusRouteBusStops(busStopBusRoute: List<BusRouteBusStop>)
}