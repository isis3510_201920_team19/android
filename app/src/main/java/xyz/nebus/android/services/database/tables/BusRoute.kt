package xyz.nebus.android.services.database.tables

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "bus_routes")
data class BusRoute(

    @PrimaryKey(autoGenerate = false)
    var id: Long,

    var name: String
)