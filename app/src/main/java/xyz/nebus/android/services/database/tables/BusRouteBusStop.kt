package xyz.nebus.android.services.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index

@Entity(
    tableName = "bus_routes_bus_stops",
    primaryKeys = ["busRouteId", "busStopId"],
    foreignKeys = [
        ForeignKey(
            entity = BusRoute::class,
            parentColumns = ["id"],
            childColumns = ["busRouteId"]
        ),
        ForeignKey(
            entity = BusStop::class,
            parentColumns = ["id"],
            childColumns = ["busStopId"]
        )
    ],
    indices = [Index(name = "bus_stop_id_index", value = ["busStopId"])]
)
data class BusRouteBusStop(

    @ColumnInfo(name = "busRouteId")
    var busRouteId: Long,

    @ColumnInfo(name = "busStopId")
    var busStopId: Long
)