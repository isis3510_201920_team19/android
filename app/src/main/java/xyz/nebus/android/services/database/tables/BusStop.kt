package xyz.nebus.android.services.database.tables

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "bus_stops")
data class BusStop(

    @PrimaryKey(autoGenerate = false)
    var id: Long,

    var name: String,

    var latitude: Double,

    var longitude: Double
)