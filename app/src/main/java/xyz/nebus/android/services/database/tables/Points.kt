package xyz.nebus.android.services.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "points")
data class Points(

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,

    @ColumnInfo(name = "point_count")
    var pointCount: Int = 100
)
