package xyz.nebus.android.services.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,

    @ColumnInfo(name = "firebase_id")
    var firebaseId: String,

    @ColumnInfo(name = "backend_id")
    var backendId: Long
)