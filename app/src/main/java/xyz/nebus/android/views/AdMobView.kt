package xyz.nebus.android.views

import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import xyz.nebus.android.screens.main.MainViewModel

class AdMobView(
    private val unitId: String,
    private val mAd: RewardedVideoAd,
    private val viewModel: MainViewModel) : RewardedVideoAdListener {

    init {
        mAd.rewardedVideoAdListener = this
    }

    fun loadAd() {
        mAd.loadAd(unitId, AdRequest.Builder().addTestDevice("DFB1FA43A2B8A53D1EABC04A78891466").build())
    }

    fun showAd() {
        if (mAd.isLoaded) {
            mAd.show()
        }
    }

    override fun onRewardedVideoAdClosed() {
        viewModel.adViewed()
    }

    override fun onRewardedVideoAdLeftApplication() {

    }

    override fun onRewardedVideoAdLoaded() {
        viewModel.adLoadedCorrectly()
    }

    override fun onRewardedVideoAdOpened() {
        viewModel.adViewed()
    }

    override fun onRewardedVideoCompleted() {
        viewModel.adViewed()
    }

    override fun onRewarded(p0: RewardItem?) {
        viewModel.adViewed()
    }

    override fun onRewardedVideoStarted() {

    }

    override fun onRewardedVideoAdFailedToLoad(p0: Int) {
        if (p0 == 3) {
            viewModel.adViewed()
        } else {
            viewModel.adFailedToLoad()
        }
    }
}