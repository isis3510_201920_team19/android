package xyz.nebus.android.views

import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.mapbox.android.core.location.LocationEngine
import com.mapbox.android.core.location.LocationEngineListener
import com.mapbox.android.core.location.LocationEnginePriority
import com.mapbox.android.core.location.LocationEngineProvider
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode
import xyz.nebus.android.R
import xyz.nebus.android.models.Bus
import xyz.nebus.android.screens.main.MainViewModel
import xyz.nebus.android.services.database.tables.BusStop


class MapboxView(
    private val fragment: Fragment,
    private val viewModel: MainViewModel,
    private val savedInstanceState: Bundle?,
    private val mapView: MapView,
    private val accessToken: String) : LifecycleObserver, PermissionsListener, LocationEngineListener {

    private lateinit var map: MapboxMap
    private lateinit var permissionManager: PermissionsManager
    var originLocation: Location? = null

    private var locationEngine: LocationEngine? = null
    private var locationLayerPlugin: LocationLayerPlugin? = null

    private var initialized = false

    private var selectedBusStopMarker: Marker? = null
    private var busMarker: Marker? = null

    init {
        fragment.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        Mapbox.getInstance(fragment.context!!, accessToken)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync { mapboxMap ->
            map = mapboxMap
            enableLocation()
            setupMarkerClickListener()

            initialized = true
        }
    }

    private fun enableLocation() {
        if (PermissionsManager.areLocationPermissionsGranted(fragment.context!!)) {
            initializeLocationEngine()
            initializeLocationLayer()
        } else {
            permissionManager = PermissionsManager(this)
            permissionManager.requestLocationPermissions(fragment.activity!!)
        }
    }

    private fun initializeLocationEngine() {
        locationEngine = LocationEngineProvider(fragment.context!!).obtainBestLocationEngineAvailable()
        locationEngine?.priority = LocationEnginePriority.HIGH_ACCURACY
        locationEngine?.activate()

        val lastLocation = locationEngine?.lastLocation
        if (lastLocation != null) {
            originLocation = lastLocation
            setCameraPosition(lastLocation)
        } else {
            locationEngine?.addLocationEngineListener(this)
        }
    }

    private fun setCameraPosition(location: Location) {
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(location.latitude, location.longitude), 19.0))
    }

    private fun initializeLocationLayer() {
        locationLayerPlugin = LocationLayerPlugin(mapView, map, locationEngine)
        locationLayerPlugin?.setLocationLayerEnabled(true)
        locationLayerPlugin?.cameraMode = CameraMode.TRACKING
        locationLayerPlugin?.renderMode = RenderMode.NORMAL
    }

    override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
        // present text explaining why permission is needed
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            enableLocation()
        }
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        permissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onLocationChanged(location: Location?) {
        location?.let {
            originLocation = location
            setCameraPosition(location)
        }
    }

    override fun onConnected() {
        locationEngine?.requestLocationUpdates()
    }

    fun setBusStops(busStops: Array<BusStop>) {
        if (initialized) {
            map.clear()
            selectedBusStopMarker = null
            busMarker = null

            for (busStop in busStops) {
                val iconFactory = IconFactory.getInstance(fragment.context!!)

                map.addMarker(
                    MarkerOptions()
                        .position(LatLng(busStop.latitude, busStop.longitude))
                        .title(busStop.name)
                        .icon(iconFactory.fromResource(R.drawable.blue_marker))
                )
            }
        }
    }

    fun selectBusStop(busStopName: String) {
        busMarker?.remove()
        busMarker = null

        for (marker in map.markers) {
            if (marker.title == busStopName) {
                selectBusStopMarker(marker)
                break
            }
        }
    }

    private fun setupMarkerClickListener() {
        map.setOnMarkerClickListener { marker ->
            if (marker.title.startsWith("en")) {
                false
            } else {
                selectBusStopMarker(marker)
                viewModel.onSelectBusStop(marker.title)
                true
            }
        }
    }

    private fun selectBusStopMarker(marker: Marker) {
        val iconFactory = IconFactory.getInstance(fragment.context!!)

        if (selectedBusStopMarker != null) {
            val defaultIcon = iconFactory.fromResource(R.drawable.blue_marker)
            selectedBusStopMarker?.icon = defaultIcon
        }

        val icon = iconFactory.fromResource(R.drawable.orange_marker)
        marker.icon = icon
        selectedBusStopMarker = marker
    }

    fun clearNonSelectedBusStopMarkers() {
        for (marker in map.markers) {
            if (marker != selectedBusStopMarker && marker != busMarker) {
                marker.remove()
            }
        }
    }

    fun addBusMarker(bus: Bus) {
        val iconFactory = IconFactory.getInstance(fragment.context!!)

        if (busMarker != null) {
            busMarker?.position = bus.location
        } else {
            busMarker = map.addMarker(
                MarkerOptions()
                    .position(bus.location)
                    .title("en " + bus.arrivesIn.toString() + "min")
                    .icon(iconFactory.fromResource(R.drawable.bus_marker))
            )
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        if (PermissionsManager.areLocationPermissionsGranted(fragment.context!!)) {
            locationEngine?.requestLocationUpdates()
            locationLayerPlugin?.onStart()
        }
        mapView.onStart()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        mapView.onResume()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        mapView.onPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        mapView.onStop()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        locationEngine?.deactivate()
    }

    fun onDestroyView() {
        mapView.onDestroy()
    }

    fun onSaveInstanceState(outState: Bundle) {
        mapView.onSaveInstanceState(outState)
    }

    fun onLowMemory() {
        mapView.onLowMemory()
    }
}